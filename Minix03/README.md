# MiniX 03 - Throbber

![](/Minix03/Panda.03.PNG)

Run the code [here](https://me-myself-and-i9231609.gitlab.io/aesthetic-programering/Minix03/index.03.html?ref_type=heads)

See the code [here](/Minix3/sketch.03.js)

If you'd like, you can change the number of pandas in my code at `numPandas` it's kind of fun to play with :)

This week we had to make a throbber. What we associated with a throbber was negative things. Irritation, anger, frustration etc. so I wanted to explore how to turn those feelings into positive feelings instead. 

##### What and why... 

To accomplish this, I've played around with different time-related features to make my throbber eye-catching. My theory was that if you were distracted by, in this case, foreheads, you would move away from the negative thoughts and focus on them instead. I used variables such as `circleSpeed` og `pandaRotationSpeed`.  

One thing that I personally have done when I've encountered a throbber - in addition to being filled with negative emotions - it always makes me press multiple times. It's a pure reflex. We think that if you press something multiple times, the desired action happens faster. However, the computer's time is inferred and what we don't see is that the computer is thinking and working its way to what we want it to do. That's why I've added the little detail to my throbber that when you hold down the mouse, the throbber will stop - to catch your attention - but the heads show that they are still thinking because they are still spinning around but on the spot instead of in a path. 
Besides, you can't be mad at pandas. 

If I were to add something to it, it would be a loading bar that would show how much time is left. This would give the user peace of mind and a further understanding that the desired action is actually in progress. 




