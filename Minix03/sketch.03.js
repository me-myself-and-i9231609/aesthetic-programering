
let numPandas = 4;
let circleRadius = 50;
let centerX, centerY;
let angle = 0;
let circleSpeed = 0.10; // Hastighed af bevægelse i cirklen
let pandaRotation = 0; // Startvinkel for hovedrotation
let pandaRotationSpeed = 0.05; // Hastighed af hovedrotation

let circleMovement = true; 

function setup() {
    createCanvas(windowWidth, windowHeight);
  centerX = windowWidth / 2;
  centerY = windowHeight / 2;
}

function draw() {
  background(195, 250, 232)

  centerX = mouseX;
  centerY = mouseY;
  let angleStep = TWO_PI / numPandas;

   // Opdater vinklen for bevægelse i cirklen
   if(circleMovement) {
    angle += circleSpeed;
   } else {
    pandaRotation += pandaRotationSpeed;
   }
   
  
   // Tegn pandabjørne i en cirkulær bane
   for (let i = 0; i < numPandas; i++) {
     let currentAngle = angle + i * angleStep;
     let xPos = centerX + cos(currentAngle) * circleRadius;
     let yPos = centerY + sin(currentAngle) * circleRadius;
     panda(xPos, yPos, 0.45);
  
}
}
function mousePressed() {
  // Når musen trykkes ned, stop cirkulær bevægelse og start rotation af pandabjørnenes hoveder rundt på stedet
  circleMovement = false;
  pandaRotation = 0;
}

function mouseReleased() {
  // Når musen frigives, genoptag cirkulær bevægelse
  circleMovement = true;
}



function panda(centerX, centerY, scalingFactor) {
  push();

  translate(centerX, centerY);
  scale(scalingFactor);

  rotate(pandaRotation);

  // the ears 
  fill(0)
  ellipse(-50, -50, 60, 50)
  ellipse(50, -50, 60, 50)

  // the head 
  fill(255, 255, 255)
  stroke(2)
  ellipse(0, 0, 150, 120)

  // the face 
  noStroke()
  fill(252, 222, 228)
  circle(35, 25, 20)
  circle(-35, 25, 20)


  fill(0)
  rect(-50, -20, 45, 45, 100, 30, 200, 200)
  rect(5, -20, 45, 45, 100, 30, 200, 200)

  fill(255, 255, 255)
  circle(25, 5, 20)
  circle(-25, 5, 20)

  fill(0)
  circle(-23, 3, 10)
  circle(26, 3, 10)

  noStroke()
  fill(180, 25, 55)
  circle(0, 40, 20)

 fill(0)
  ellipse(0, 20, 10, 10)
  pop();


}




