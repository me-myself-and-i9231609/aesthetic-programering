# MiniX 04 - The Zuckerberg experience

![](/MInix04/front.jpg)

Run the code [here](https://me-myself-and-i9231609.gitlab.io/aesthetic-programering/MInix04/Minix04.html)

See the code [here](/MInix04/sketch04.js)

##### Data Harvest - The Zuckerberg experience
Step into a world where every click, every keystroke, every movement is a piece in the grand puzzle of data collection. Welcome to "Data Harvest: The Zuckerberg Experience" - an interactive art piece exploring the boundaries between privacy and profit. 

At first glance, it might seem like just an ordinary Facebook front page. A familiar blue backdrop, a logo, an input field for your information. But what sets this page apart from the usual social media experience is the subtle yet chilling message awaiting those brave enough to engage: "Facebook: Where your information is our income and Zuckerberg's goldmine."

To embark on your journey, simply type your information into the field and hit enter. And here begins the game. Each time you press, a dollar sign appears along with a GIF of Mark Zuckerberg echoing his mantra: "More, more, more, not less."

This is a commentary on modern reality, where our data has become a commodity, a source of immense wealth for a select few. "Data Harvest" challenges us to consider the costs of our online presence and what it means to become part of the vast network.

So come and give it a try. Input your information. Feel the rush of contributing to a billion-dollar enterprise. Welcome to "Data Harvest: The Zuckerberg Experience".

##### What and why... 

This code is long, but it's not hard to understand. The first part is a series of 'DOM elements' that allow you to interact with the website. I would say it's a simple way to create input fields and style them, however, it's the part that makes the code long. 

I've used the `forEach` variable to make the experience a bit more authentic and make some input fields that only appear when the mouse is inside the field and pressed. And remain displayed when you have typed something in it.  

I have for the first time used the function `preload()` which makes it possible to run the elements the function contains, i.e. `img2, img1, gifAnimation` before the `draw()` function itself. 


