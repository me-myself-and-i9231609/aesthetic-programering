// billederne
let img1, imag2, img3;
let showImage = false; // vi sætter den til falsk så den ikke vises hele tiden (det ændre vi senere)
let showImageTime = 0;
let imageDisplayDutation = 100;

// textbox login
let input1;
let input2;

// textbox signup 
let input3;
let input4;
let input5;
let input6;
let input7;
let input8;
let input9;

//dropdown
let dropdown1;

// signup knap 
let button;

// gif
let gifAnimation;
let gifX, gifY; // posistionen for gif'en 
let gifSpeedX, gifSpeedY; // hastigheden for gif'en
let gifWidth = 250;
let gifHeight = 200;

//tekste 
let text1;
let text1X = 30;
let text1Y = 500;

// sloganet 
let additionalText = "Facebook: Where your information is our income and Zuckerberg's goldmine.";
let showAdditionalText = false;
let additionalTextX = 30;
let additionalTextY = 550

function setup() {
  createCanvas(1500, 1000);
  background(200);

  input1 = createInput();
  input1.hide();
  input1.position(650, 31); // den skal hedde posistion og ikke style eller virker den ikke. 
  input1.style('display', 'flex');
  input1.style('width', '135px')
  input1.style('height', '8px')
  input1.style('padding', '5px');

  input2 = createInput();
  input2.hide();
  input2.position(810, 31); // den skal hedde posistion og ikke style eller virker den ikke. 
  input2.style('display', 'flex');
  input2.style('width', '139px')
  input2.style('height', '8px')
  input2.style('padding', '5px');

  input3 = createInput();
  input3.hide();
  input3.position(630, 205); // den skal hedde posistion og ikke style eller virker den ikke. 
  input3.style('display', 'flex');
  input3.style('width', '180px')
  input3.style('height', '20px')
  input3.style('padding', '5px');

  input4 = createInput();
  input4.hide();
  input4.position(835, 205); // den skal hedde posistion og ikke style eller virker den ikke. 
  input4.style('display', 'flex');
  input4.style('width', '180px')
  input4.style('height', '20px')
  input4.style('padding', '5px');

  input5 = createInput();
  input5.hide();
  input5.position(630, 250); // den skal hedde posistion og ikke style eller virker den ikke. 
  input5.style('display', 'flex');
  input5.style('width', '384px')
  input5.style('height', '20px')
  input5.style('padding', '5px');

  input6 = createInput();
  input6.hide();
  input6.position(630, 300); // den skal hedde posistion og ikke style eller virker den ikke. 
  input6.style('display', 'flex');
  input6.style('width', '384px')
  input6.style('height', '20px')
  input6.style('padding', '5px');

  input7 = createInput();
  input7.hide();
  input7.position(630, 350); // den skal hedde posistion og ikke style eller virker den ikke. 
  input7.style('display', 'flex');
  input7.style('width', '384px')
  input7.style('height', '20px')
  input7.style('padding', '5px');

  dropdown1 = createSelect();
  dropdown1.hide();
  dropdown1.position(630, 426);
  dropdown1.style('display', 'flex');
  dropdown1.style('width', '73px');
  dropdown1.style('height', '25px');
  dropdown1.option('Jan');
  dropdown1.option('Feb');
  dropdown1.option('Mar');
  dropdown1.option('Apr');
  dropdown1.option('Maj');
  dropdown1.option('Jun');
  dropdown1.option('Jul');
  dropdown1.option('Aug');
  dropdown1.option('Sep');
  dropdown1.option('Okt');
  dropdown1.option('Nov');
  dropdown1.option('Dec');

  input8 = createInput();
  input8.hide();
  input8.position(710, 426);
  input8.style('display', 'flex');
  input8.style('width', '45px');
  input8.style('height', '23px');

  input9 = createInput();
  input9.hide();
  input9.position(770, 426);
  input9.style('display', 'flex');
  input9.style('width', '54px');
  input9.style('height', '23px');

  button = createButton('Sign up');
  button.position(629, 555)
  button.style('width', '195px');
  button.style('height', '38px');
  button.style('border-radius', '10px');
  button.style('background-color', '#119131'); // her ville rgb farver ikke virke der skal bruges ord eller # farver
  button.style('font-size', '20px');

  // Initialiser startposition og hastighed for gif'en
  gifX = 1600;
  gifY = 300;
  gifSpeedX = 0;
  gifSpeedY = -5;

  // Opret tekstobjekter
  text1 = createP('Try it and sign in!');
  text1.position(30, 500);
  text1.style('font-size', '100px');
  text1.style('color', '#2e69f2');

  additionalText = createP("(Facebook: Where your information is our income and Zuckerberg's goldmine.)");
  additionalText.position(30, 700);
  additionalText.style('font-size', '30px');
  additionalText.style('color', '#8e9cbd');
}

function preload() { // en preload gøre at programmet kører før draw og setup. 
  img2 = loadImage('Blue-dollar-sign.PNG')
  img1 = loadImage('facebook.accont.jpg')
  gifAnimation = loadImage('markgif.gifimage.gif')



}
function changeImage() {
  background(200);
  image(img3, 500, 500);
}

function draw() {
  background(200);
  image(img1, -100, 0);
  image(gifAnimation, gifX, gifY, gifWidth, gifHeight);

  if (showImage) {
    image(img2, mouseX, mouseY, 50, 50);
  }

  if (millis() > showImageTime) {
    showImage = false;
  }

  gifX += gifSpeedX;
  gifY += gifSpeedY;

}

function mousePressed() {
  showImage = true;
  showImageTime = millis() + imageDisplayDutation;

  let inputs = [input1, input2, input3, input4, input5, input6, input7, input8, input9];
  // Skjul alle felterne, undtaget dem der er skrevet i 
  inputs.forEach(input => {
    if (input.value() === "") {
      input.hide();
    }
  });
  // Vis det relevante felt, hvis musen er inden for dets område
  inputs.forEach(input => {
    if (
      mouseX >= input.x &&
      mouseX <= input.x + input.width &&
      mouseY >= input.y &&
      mouseY <= input.y + input.height
    ) {
      input.show();
    }
  });

  // Vis dropdown, hvis musen er inden for dens område
  if (
    mouseX >= dropdown1.x &&
    mouseX <= dropdown1.x + dropdown1.width &&
    mouseY >= dropdown1.y &&
    mouseY <= dropdown1.y + dropdown1.height
  ) {
    dropdown1.show();
  }

  // start gif'en forfra og nulstil position og brug den oprindelig hasighed 
  gifX = 1200
  gifY = 500; // start neders på skærmen 
  gifSpeedX = 0; // ingen horizontal bevægelse 
  gifSpeedY = random(-10, -5);


}