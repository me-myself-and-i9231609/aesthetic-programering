# MiniX 05 - Bubble rush 

![](/Minix05/bubblerush.PNG)

Run the code [here](https://me-myself-and-i9231609.gitlab.io/aesthetic-programering/Minix05/index05.html?ref_type=heads)

See the code [here](/Minix05/sketch05.js)


– What are the rules in your generative program? Describe how your program
performs over time? How do the rules produce emergent behavior?
Bubble rush is an interactive code that follows two rules; 
1. When a bubble hits another bubble, the bubble gets smaller 

2. When a bubble hits the bottom of the screen, the bubble gets bigger. 

You create new bubbles by tapping the mouse. It works almost like a pastime game, where you have to make sure that a single bubble doesn't take over your entire screen. After the code has been running for a while and you've made enough bubbles, the bubbles grow a lot. To get them down again, you have to make sure that more bubbles fall on the growing one. 


– What role do rules and processes have in your work?
– Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via
rules)? Do you have any further thoughts on the theme of this chapter?

`preload()`

