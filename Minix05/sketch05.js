class Circle {
  constructor(x, y, diameter) {
    this.x = x;
    this.y = y;
    this.diameter = diameter;
    this.speedX = random(-2, 2);
    this.speedY = random(-2, 2);
    this.color = color(random(255), random(255), random(255));
  }

  display() {
    fill(this.color);
    ellipse(this.x, this.y, this.diameter);
  }

  move() {
    this.x += this.speedX;
    this.y += this.speedY;

    if (this.x <= 0 || this.x >= width) {
      this.speedX *= -1;
    }

    // Check if the bottom of the circle reaches the bottom of the window
    if (this.y + this.diameter / 2 >= height) {
      this.diameter -= 2;
      if (this.diameter < 0) {
        this.diameter = 0;
      }
      this.speedY *= -1; // Reverse the Y direction when hitting the bottom
    }

    // Reduce the diameter of the circle if it hits the bottom of the window
    if (this.y >= height) {
      this.diameter += 4;
      if (this.diameter < 0) {
        this.diameter = 0;
      }
    }
  }

  checkCollision(otherCircles) {
    for (let i = 0; i < otherCircles.length; i++) {
      let otherCircle = otherCircles[i];
      let distance = dist(this.x, this.y, otherCircle.x, otherCircle.y);
      let minDist = this.diameter / 2 + otherCircle.diameter / 2;
      if (distance <= minDist && this != otherCircle) {
        let dx = this.x - otherCircle.x;
        let dy = this.y - otherCircle.y;
        let angle = atan2(dy, dx);
        this.speedX = cos(angle) * abs(this.speedX);
        this.speedY = sin(angle) * abs(this.speedY);
        this.x = otherCircle.x + cos(angle) * minDist;
        this.y = otherCircle.y + sin(angle) * minDist;

        // Increase the diameter of this circle by 1
        this.diameter -= 2;
      }
    }
  }

  applyGravity() {
    // Apply gravitational force to the speed in the Y direction
    this.speedY += gravity;
  }
}

let circles = []; // Array to hold all the circles
let gravity = 0.1; // Gravitational force

function setup() {
  createCanvas(windowWidth, windowHeight);
  // Create two circles at startup
  circles.push(new Circle(width / 2, height / 2, random(20, 50)));
  circles.push(new Circle(width / 2 + 50, height / 2 + 50, random(20, 50)));
}

function draw() {
  background(100, 100, 200);

  // Draw and update all the circles
  for (let i = 0; i < circles.length; i++) {
    circles[i].display();
    circles[i].move();
    circles[i].checkCollision(circles.slice(0, i)); // Check for collision with previous circles
    circles[i].applyGravity(); // Apply gravitational force
  }
}

function mouseClicked() {
  let newCircle = new Circle(mouseX, mouseY, random(20, 50)); // Create a new circle at the mouse position
  circles.push(newCircle); // Add the new circle to the array
}
