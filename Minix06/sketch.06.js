// global variables
let ball;
let levels = []; // an array to store the level objects in the game
let lives = 3;
let gameOver = false; // a boolean variable to track whether the game is over or not
let backgroundImage;

function preload() {
  // Indlæs dit billede i preload-funktionen
  backgroundImage = loadImage('back.jpg');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  ball = new Ball(); // it initializes the ball variable with a new instance of the Ball class
  levels.push(new Level()); // it adds the first level object to the levels array
 }

function draw() {
  background(backgroundImage);

  // it displays the number of lives remaining at the top-left corner of the canvas
  textSize(25);
  fill(255);
  noStroke();
  textAlign(CENTER);
  text("Lives: " + lives, 20, 30);
     
  if (!gameOver) { // if the game is not over
    for (let i = levels.length - 1; i >= 0; i--) { // it updates and displays each level in the levels array
      levels[i].show();
      levels[i].update();

      // it checks for collisions between the ball and levels, and updates the game state accordingly
      if (levels[i].hits(ball)) {
        if (!levels[i].hit) { // check if the level has already been hit in this frame
          lives--;
          levels[i].hit = true; // set the hit flag to true to prevent multiple decrement for the same level
          if (lives === 0) {
            gameOver = true;
          }
        }
        levels[i].highlight = true;
      }

      if (levels[i].offscreen()) {
        levels.splice(i, 1);
      }
    }

    // it updates and displays the ball's position
    ball.update();
    ball.show();

    // it adds a new level to the levels array every 75 frames
    if (frameCount % 75 == 0) {
      levels.push(new Level());
    }
  } else { // if the game is over
    // it displays the game over screen with the message "You lost! Press SPACE to restart."
    let gameOverScreen = new GameOverScreen("You lost;( Press the SPACE-bar to tyr again!");
    gameOverScreen.show();
  }
}

function keyPressed() {
  if (gameOver && key == ' ') { // if the game is over and the spacebar is pressed, it calls the restartGame() function to restart the game
    restartGame();
  } else if (keyCode === LEFT_ARROW) { // if the left arrow key is pressed, move the ball left
    ball.moveLeft();
  } else if (keyCode === RIGHT_ARROW) { // if the right arrow key is pressed, move the ball right
    ball.moveRight();
  }
}

function restartGame() { // this function resets the game state when called
  gameOver = false;
  lives = 3;
  levels = [];
  ball = new Ball();
  levels.push(new Level());
  // it sets gameOver to false, resets the number of lives to 3, clears the levels array, and initializes a new ball object and a new level object
}

// ball class
class Ball {
  constructor() {
    this.y = height / 2;  // the initial y-coordinate of the ball, set near the bottom of the canvas
    this.x = width / 2; // the initial x-coordinate of the ball, set to the middle of the canvas
    this.speed = 30; // the horizontal speed at which the ball moves
    this.diameter = 20; // the diameter of the ball
  }

  show() { // this method is responsible for displaying the ball on the canvas
    fill(255);
    ellipse(this.x, this.y, this.diameter, this.diameter);
  }

  moveLeft() { // this method is called when the left arrow key is pressed to move the ball left
    this.x -= this.speed;
    this.x = constrain(this.x, 0 + this.diameter / 2, width - this.diameter / 2); // constrain the ball's x-coordinate within the canvas width
  }

  moveRight() { // this method is called when the right arrow key is pressed to move the ball right
    this.x += this.speed;
    this.x = constrain(this.x, 0 + this.diameter / 2, width - this.diameter / 2); // constrain the ball's x-coordinate within the canvas width
  }
  update() {
  }
}

// game over screen class
class GameOverScreen {
  constructor(message) {
    this.message = message;
  }

  show() {
    background(0, 160);
    textAlign(CENTER);
    fill(255, 0, 0);
    textSize(30);
    stroke(0);
    strokeWeight(5);
    text('GAME OVER', width / 2, height / 2);
    fill(255);
    textSize(20);
    text(this.message, width / 2, height / 2 + 30);
  }
}

// level class
class Level {
  constructor() {
    this.gap = 100; // the gap between the levels
    this.x1 = random(width / 2); // the initial x-coordinate of the first rectangle
    this.x2 = random(width / 2, width); // the initial x-coordinate of the second rectangle
    this.y = height; // the initial y-coordinate of the level (bottom of the screen)
    this.w = random(10, width / 2); // the width of the first rectangle
    this.speed = 2; // the vertical speed at which the level moves
    this.highlight = false; // a boolean flag indicating whether the level should be highlighted (e.g., when it's hit by the ball)
    this.hit = false; // flag to track if the level has been hit by the ball
  }

  hits(ball) { // this method checks if the ball has collided with the level. It takes the ball object as an argument
    if ((ball.x + ball.diameter / 2 > this.x1 && ball.x - ball.diameter / 2 < this.x1 + this.w) ||
        (ball.x + ball.diameter / 2 > this.x2 && ball.x - ball.diameter / 2 < this.x2 + this.w)) {
      if (ball.y + ball.diameter / 2 > this.y) { // check if the ball's y-coordinate is below the level
        this.highlight = true; // if a collision occurs, set the highlight property of the level to true to visually indicate the collision
        return true; // return true to indicate a collision
      }
    }
    this.highlight = false; // if no collision occurs, set highlight to false
    return false; // return false to indicate no collision
  }

  show() { // this method is responsible for displaying the level on the canvas
    fill(255); // set the fill color to white
    if (this.highlight) { // if the level is highlighted (hit by the ball), change the fill color to red
      fill(255, 0, 0);
    }
    rect(this.x1, this.y, this.w, 10); // draw the first rectangle
    rect(this.x2, this.y, this.w, 10); // draw the second rectangle
  }

  offscreen() { // this method checks if the level is offscreen to the top
    return this.y < 0;
  }

  update() { // this method updates the position of the level
    this.y -= this.speed; // move the level vertically upwards

    if (this.offscreen()) { // if the level is offscreen to the top, remove it from the array
      levels.splice(1, 1);
    }
  }
}