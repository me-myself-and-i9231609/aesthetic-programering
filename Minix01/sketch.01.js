let x1 = 0;
let x2 = 720;
let y1 = 0;
let y2 = 400;
let backgroundColor = 0;


// The statements in the setup() function
// execute once when the program begins
function setup() {
  createCanvas(720, 400);
  stroke(255);
  noLoop();
}

// The statements in draw() are executed until the  program is stopped. Each statement is executed in sequence and after the last line is read, the first line is executed again.
function draw() {
  background(backgroundColor);

  
  x1 = x1 + 1; // Line from the rigth moves because it has +1 variabele, so the line moves +1 every time the draw runs
  if (x1 > width) { // here the if statment is that if the x1 posistion is bigger than the widht of the window, then it sets x1 back to 0 and updates the backgroung color for a random color. 
    x1 = 0;
    updateBackgroundColor();
  }
  line(x1, height, x1, 0);

  // line  from the left 
  x2 = x2 - 1;
  if (x2 < 0) {
    x2 = width;
    updateBackgroundColor();
  }
  line(x2, height, x2, 0);

  // Line from he top 
  y1 = y1 + 1;
  if (y1 > height) {
    y1 = 0;
    updateBackgroundColor();
  }
  line(0, y1, width, y1);

  // line from the bottom 
  y2 = y2 - 1;
  if (y2 < 0) {
    y2 = height;
    updateBackgroundColor();
  }
  line(0, y2, width, y2);
}

// this function creates a loop, where the lines starts to move afte mouse is pressed. 
function mousePressed() {
  loop(); // the loop here i nessesery because this makes the interaction between the user and the program. if we did'nt have it, the program would start by itself.
  // restarts the coordinater og frameRate of the lines fom the sides 
  x1 = 0;
  x2 = width;
  y1 = 0;
  y2 = height;
  frameRate(50);
}

// this function changes the backgroundcolor 
function updateBackgroundColor() {
  backgroundColor = color(random(255), random(255), random(255));
}
