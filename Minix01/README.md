### How it looks
![](/Minix01/Udklip.PNG)

To run the code [here](http://127.0.0.1:5500/workspace/MiniX%2002/index.02.html)

See the code [here!](/Minix01/sketch.js)

#####  What have you produced?
I created this code that plays with lines on either side of the canvas. 

The lines go towards each other, meet, cross, and then travel to the other side. When it reaches the other side, the background color will change to a new random color. 

I made it using javascript (obviously) where I first went to the p5.js library to find some inspiration. I found the `loop()` function very interesting. Therefore, I made a remix of the example already on the ps.js library. 

I wanted to keep track of the different parts, so it was not important for me to make many different things this time. Instead, I wanted to do one or two things in different ways so I could understand how the elements worked. 

I played with `if` and how to make the line move, what difference it made when I replaced `+`with `-` or how a `loop()` works in general. 



##### How would you describe your first independent coding experience 
The first MiniX01 is not complicated, but I have a better understanding of the individual parts. 

I agree one hundred percent with the book that programming is "fun and rewarding but also annoying and frustrating." 

I've struggled a lot with adding more advanced parts to my code, but nothing has been successful enough to make it into the final MInix01. It's frustrating, annoying, unpredictable when it will be across, but it also gives a satisfaction when it finally succeeds. Because it is often difficult and not easy, you are surprised and proud when you finally succeed. So for now, I still have the courage to take on the next task. 


