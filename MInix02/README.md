# Minix 2 

### My Emojies: 

![](/MInix02/minix2.PNG)

[See the code here](/MInix02/sketch.02.js)

(If you want to run the code, you have to copy the js ang the html files into your own VS. But there is no interaction in my code.) 

#### My program 

My two emojies are a teepee and an igloo. They are all individually made from geometric shapes. The teepee is heavily influenced by triangles, while the igloo is influenced by squares and lines. 
It may not be very special to look at, but I've learned a lot from making it. I've used new functions like `curve()` and `patterenSpacing()` and also found out that you have to have the "style", i.e. the color, stroke etc. must be before the figure itself, because the figure is based on what is above it. Although it sounds banal, it wasn't obvious (to me) because in html and css it works in a slightly different way. 

My minix 02 is not very interesting for other people because it is not interactive. But it was a good exercise because you played a lot with `x, y, h, s, [tr], [tl]` ect. 


#### How would you put your emoji into a wider social and cultural context? 

I chose these two emojis because I realized they didn't exist. I wondered how it could be that they hadn't been made, when you make religious and cultural emojis for other cultures. It reminded me of the debate in Denmark some time ago about no longer being allowed to dress up as, for example, an Indian or a Muslim, and that you could no longer call it an Eskimo ice cream, etc. 

I can understand being offended if people do the aforementioned things out of hate or to make fun of a culture. But most of the time, that's not the case. Additionally, I would argue that because these two emojis don't exist in reality, it can be off-putting to those who actually use teepees and igloos, or just see them as part of their identity and culture. That's why I made them. 




