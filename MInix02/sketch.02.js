function setup() {
  // put setup code here
  createCanvas(1000, 1000);
}

function draw() {
  background(240); // Set background color

  
  // The tipi emojie 
  
  // Base of tipi
  fill(247, 204, 129); // Brown color
  stroke(0); // Black stroke
  strokeWeight(4);
  triangle(30, 270, 115, 60, 200, 270);

  // Tipi opening
  fill(0); // Black color for the rectangle
  noStroke(); // No stroke for the rectangle
  rect(77, 210, 70, 60, 50, 50, 0, 0);

  // Sticks on top
  stroke(0);
  strokeWeight(3);
  line(90, 30, 115, 60);
  line(115, 60, 150, 30);
  line(115, 60, 115, 30);

  // Pattern around the tipi (inside the triangle)
  let triangleSize = 10; // Adjust the size of the triangles
  let patternSpacing = 15; // Adjust the spacing between pattern elements

  for (let x = 63; x <= 160; x += patternSpacing) {
    fill(255, 51, 51);
    stroke(0);
    strokeWeight(2);
    triangle(x, 190, x + triangleSize, 190, x + triangleSize / 2, 170);
  }

  // Pattern around the tipi (second triangle with a different starting position)
  let patternSpacing2 = 15; // Adjust the spacing between pattern elements
  let startX2 = 250; // Adjust the starting x-position for the second pattern

  for (let x = 70; x <= 150; x += patternSpacing2) {
    fill(255, 51, 51);
    strokeWeight(1);
    triangle(x, 190 + -30, x + 10, 190 + - 30, x + 5, 210 + -30);
  }


  // The Igloo emojie 

  // Snow 
  noStroke()
  fill(243, 255, 255)
  ellipse(450, 270, 300, 80)

  // Igloo base form 
  strokeWeight(4);
  stroke(200, 250, 255);
  fill(250, 255, 255);
  rect(380, 120, 190, 150, 400, 250, 50, 30);

  // the lines on the igloo 
  noFill()
  curve(380, 80, 547, 140, 403, 140, 570, 80)
  curve(380, 80, 565, 180, 390, 180, 570, 80)
  curve(380, 80, 567, 220, 385, 220, 570, 80)
  line(450, 145, 460, 120)
  line(500, 145, 490, 120)
  line(410, 190, 420, 145)
  line(475, 190, 475, 147)
  line(540, 190, 525, 147)
  line(457, 194, 455, 237)
  line(520, 194, 525, 235)
  line(490, 237, 490, 270)


  // the tunnel 
  stroke(200, 255, 255);
  strokeWeight(4);
  fill(200, 240, 255);
  rect(340, 180, 100, 90, 200, 200, 0, 0);
  line(390, 245, 440, 245)
  line(390, 200, 430, 200)

  // the igloo entrence
  fill(0, 170, 176);
  strokeWeight(4);
  rect(340, 180, 60, 88, 200, 200, 0, 0);

}
